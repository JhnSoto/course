'use strict'

var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var broserSync = require('browser-sync');
var del = require('del');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');
var cleanCss = require('gulp-clean-css');
var flatmap = require('gulp-flatmap');
var htmlmin = require('gulp-htmlmin');

gulp.task('sass', function(done){
	gulp.src('./css/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'));
		done();
});

gulp.task('sass:watch', function(){
	gulp.watch('./css/*.scss', gulp.series('sass'));
});

gulp.task('browser-sync', function(){
	var files = ['./*,html', './css/*.css', './images/*.{png, jpg, jpeg, gif}', './js/*.js']
	broserSync.init(files, {
		server: {
			baseDir: './'
		}
	});
});

gulp.task('default', gulp.parallel('browser-sync','sass:watch'));

gulp.task('clean', function(){
	return del(['dist']);
});

gulp.task('copyfont2', function(done){
	gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/*')
	.pipe(gulp.dest('./dist/webfonts'));
	done();
})

gulp.task('imagemin', function(){
	return gulp.src('./images/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
});

gulp.task('usemin', function(){
	return gulp.src('./*.html')
		.pipe(flatmap(function(stream, file){
			return stream
				.pipe(usemin({
					css: [rev()],
					html: [function(){return htmlmin({collapseWhitespace:true})}],
					js: [uglify(), rev()],
					inlinejs: [uglify()],
					inlinecss: [cleanCss(), 'concat']
				}))
		}))
		.pipe(gulp.dest('dist/'));
});

gulp.task('build', gulp.series('clean', 'copyfont2', 'imagemin', 'usemin'));
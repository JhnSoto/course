var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
});
 
var myModalEl = document.getElementById('exampleModal');
myModalEl.addEventListener('show.bs.modal', function (event) {
  console.log("se está mostrando");
  document.getElementById("btnsuscribir").classList.remove("btn-outline-dark"); 
  document.getElementById("btnsuscribir").classList.add("btn-primary");
  document.getElementById("btnsuscribir").disabled = true;
});
myModalEl.addEventListener('shown.bs.modal', function (event) {
  console.log("se mostró")
});
myModalEl.addEventListener('hide.bs.modal', function (event) {
  console.log("se está ocultando");
  document.getElementById("btnsuscribir").classList.add("btn-outline-dark");
  document.getElementById("btnsuscribir").classList.remove("btn-primary"); 
  document.getElementById("btnsuscribir").disabled = false;
});
myModalEl.addEventListener('hidden.bs.modal', function (event) {
  console.log("se ocultó");
 
});